<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cidadao")
 */
class Cidadao
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=11)
     * 
     */
    private $nis;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    public function getNis()
    {
        return $this->nis;
    }

    public function setNis($nis)
    {
        $this->nis = $nis;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}