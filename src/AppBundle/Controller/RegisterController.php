<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cidadao;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterController extends Controller
{
    /**
     * @Route("/register", name="registerpage")
     */
    public function indexAction(Request $request)
    {
        $cidadao = new Cidadao();
        $cidadao->setNis('00000000000');
        $cidadao->setName('Name');

        $registerForm = $this->createFormBuilder($cidadao)
            ->add('nis', TextType::class, array(
                'label' => 'NIS:',
                'required' => true
            ))
            ->add('name', TextType::class, array(
                'label' => 'Name:',
                'required' => true
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Cadastrar Cidadão'
            ))
            ->getForm();

        $registerForm->handleRequest($request);

        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $cidadao->setNis($registerForm->get('nis')->getData());
            $cidadao->setName($registerForm->get('name')->getData());

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($cidadao);

            $entityManager->flush();
            // $cidadao = $registerForm->getData();
            return $this->render('register/index.html.twig', array(
                'registerForm' => $registerForm->createView(),
                'registeredCidadao' => $cidadao,
            ));
        }   

        return $this->render('register/index.html.twig', array(
            'registerForm' => $registerForm->createView(),
        ));
    }
    
}
