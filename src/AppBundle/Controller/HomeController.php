<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cidadao;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{
    /**
     * @Route("/home", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('home/index.html.twig');
    }
    
}
