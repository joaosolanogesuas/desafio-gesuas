<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cidadao;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="searchpage")
     */
    public function showAction(Request $request)
    {
        $cidadao = new Cidadao();
        $cidadao->setNis('00000000000');
        $cidadao->setName('Name');

        $searchForm = $this->createFormBuilder($cidadao)
            ->add('nis', TextType::class, array(
                'label' => 'NIS:',
                'required' => true,
            ))
            ->add('search', SubmitType::class, array(
                'label' => 'Procurar Cidadão'
            ))
            ->getForm();

        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $nis = $searchForm->get('nis')->getData();

            $cidadao = $this->getDoctrine()
                ->getRepository(Cidadao::class)
                ->find($nis);
            
            if (!$cidadao) {
                throw $this->createNotFoundException(
                    'No citizen found for nis '.$nis
                );
            }

            return $this->render('search/index.html.twig', array(
                'searchForm' => $searchForm->createView(),
                'cidadao' => $cidadao,
            ));
        }

        return $this->render('search/index.html.twig', array(
            'searchForm' => $searchForm->createView(),
        ));
    }
    
}
